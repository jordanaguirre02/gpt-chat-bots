import requests

def getUserList(channel):
    user_list = []
    user_data = requests.get(f"https://tmi.twitch.tv/group/user/{channel}/chatters")
    print(user_data)
    user_dict = user_data.json()["chatters"]
    for user_type in user_dict.keys():
        temp_user_list = user_dict[user_type]
        for user in temp_user_list:
            user_list.append(user)
    return user_list

print(getUserList())
