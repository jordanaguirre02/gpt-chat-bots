import openai
from Keys.keys import OPEN_AI_KEY
from pvrecorder import PvRecorder
import wave
import struct
from user_list import getUserList


recorder = PvRecorder(device_index=1, frame_length=512)
audio = []
openai.api_key = OPEN_AI_KEY
user_list = getUserList()


try:
    recorder.start()

    while True:
        frame = recorder.read()
        audio.extend(frame)
except KeyboardInterrupt:
    recorder.stop()
    with wave.open("shoutout.wav", 'w') as f:
        f.setparams((1, 2, 16000, 512, "NONE", "NONE"))
        f.writeframes(struct.pack("h" * len(audio), *audio))
finally:
    recorder.delete()

audio_file = open("shoutout.wav", "rb")
transcript = openai.Audio.transcribe(
    "whisper-1",
    audio_file,
    )
voice_text = transcript["text"].lower()



prompt = f"Please take this audio transcription return text: \n{voice_text}\n Make a best guess for which user was said, based on this user list: \n{user_list}\n return a single user name."
shoutout_user = openai.ChatCompletion.create(
    model="gpt-3.5-turbo",
    messages=[{
    "role": "user",
    "content": prompt
    }]
)

print(len(user_list))
print(user_list)
print(f"Voice text::: {voice_text}")
print(shoutout_user)
