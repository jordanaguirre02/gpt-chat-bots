

const WebSocket = require('ws')
const {oAuth , nick , channel, otoken } = require('./Keys/keys.js');



const socket = new WebSocket("wss://irc-ws.chat.twitch.tv:443");

socket.addEventListener('open', () => {
    socket.send(`PASS oauth:${oAuth}`);
    socket.send(`NICK ${nick}`);
    socket.send(`JOIN #${channel}`);
});

let request = require('request');
global.user_data;
request.get(
    "https://tmi.twitch.tv/group/user/slazkio/chatters",
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            return global.user_data = body;
        }
    }
);

console.log(global.user_data);

socket.addEventListener('message', event => {
    console.log(event.data);
    if (event.data.includes("subscribed")) {
        socket.send(`PRIVMSG #${channel} :!so name`);
    }
})
