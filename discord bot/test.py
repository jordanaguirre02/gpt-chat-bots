import openai
import requests
from Keys.keys import BOT_TOKEN, OPEN_AI_KEY


openai.api_key = OPEN_AI_KEY
headers = {"authorization": BOT_TOKEN}

url = "https://cdn.discordapp.com/attachments/1089431744018657390/1093395911599734795/mountain.png"
data = requests.get(url=url, headers=headers).content
response = openai.Image.create_variation(
    image=data,
    n=2,
    size="512x512",
)
response_vari = response.get("data")
print(response_vari[0]["url"])
