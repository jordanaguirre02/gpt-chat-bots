import discord
from discord.ext import commands
import help_text
from os.path import isfile
from discord.ext.commands import HelpCommand
from Keys import keys
from ID.chan_id import general_id, gpt_id, test_id
from chatgpt_ai import gptCommands


def run_bot():
    chan_id = test_id
    gen_id = general_id
    askgpt_id = gpt_id
    intents = discord.Intents.default()
    intents.message_content = True
    intents.members = True
    bot = commands.Bot(command_prefix="!", intents=intents, help_command=None)


    @bot.event
    async def on_ready():
        bot.channel = bot.get_channel(askgpt_id)
        print(f"Welcome {bot.user}, You are assigned to {bot.channel}")

    @bot.event
    async def on_message(message):
        if message.author == bot.user:
            return
        if message.channel == bot.channel:
            message.content = str(message.content).lower()
            await gptCommands(message)
            # ctx = await bot.get_context(message)
            # if ctx.valid:
            #     await bot.process_commands(message)

    # @bot.command()
    # async def askgpt(ctx):
    #     return_message = ctx.message
    #     await ctx.channel.send(return_message)

    # @bot.command(name="xxxxisjjsjdisjd")
    # async def create(ctx, race, role):
    #     race.description = "testing"
    #     role.description = "I think i got it?"
    #     if isfile(f"Users/{ctx.author}.txt") is False:
    #         combat_xp_mod = 1.3
    #         damage_bonus_mod = 1.4
    #         user_dict = {"username": str(ctx.author),"race": race,"role": role, "cmbt_xp": combat_xp_mod, "bonus_dmg": damage_bonus_mod}
    #         with open(f"Users/{ctx.author}.txt", "a") as data:
    #             data.write(f"{user_dict}")
    #         await ctx.channel.send(f"user: @{ctx.author} created")
    #     else:
    #         await ctx.channel.send(f"Sorry, User: @{ctx.author} already exists")

    # @bot.command(name="ssjdijjjejjfnanndknnn")
    # async def load(ctx):
    #     with open(f"Users/{ctx.author}.txt") as data:
    #         data_str = data.read()
    #         user = eval(data_str)
    #         username = user["username"]
    #         race = user["race"]
    #         role = user["role"]
    #         cmbt_xp = user["cmbt_xp"]
    #         bonus_dmg = user["bonus_dmg"]
    #     await ctx.channel.send(
    #         f"hello {username}, the mighty {race} {role} from far away\n" +
    #         f"Your combat xp bonus is {cmbt_xp}x\nYou do {bonus_dmg}x more damage"
    #         )



    bot.run(keys.BOT_TOKEN)
