import openai
import requests
from Keys.keys import OPEN_AI_KEY, BOT_TOKEN


openai.api_key = OPEN_AI_KEY
headers = {
    "authorization": BOT_TOKEN
}

def chatgpt_response(user, prompt):
    response = openai.ChatCompletion.create(
        model = "gpt-3.5-turbo",
        messages = [{"role": "user", "content": str(prompt)}],
        temperature = 0.6,
        user = f"{user}",
        max_tokens = 3800
    )
    response_dict = response.get("choices")
    if response_dict and len(response_dict) > 0:
        prompt_response = response_dict[0]["message"]["content"]
        return prompt_response


def dalle_response(user, prompt):
    size = "512x512"
    response = openai.Image.create(
        prompt = prompt,
        n = 3,
        size = size,
        user = f"{user}"
    )
    response_pics = response.get("data")
    if response_pics and len(response_pics) > 0:
        return response_pics


def dalle_vari(user, image):
    response = openai.Image.create_variation(
        image = image,
        n = 2,
        size = "512x512",
        user = f"{user}",
    )
    response_vari = response.get("data")
    if response_vari and len(response_vari) > 0:
        return response_vari


async def gptCommands(message):
    try:
        message_user = message.author
        for text in ["/edit","/gpt", "/vari", "/draw"]:
            if message.content.startswith(text):
                command = message.content.split(" ")[0]
                dal_prompt = message.content.replace(text, "")
                image_url = str(message.attachments[0].url)
                print(image_url)
                data = requests.get(url=image_url, headers=headers).content
        if command == "/gpt":
            bot_response = chatgpt_response(user=message_user, prompt=dal_prompt)
            if len(bot_response) <= 2000:
                await message.channel.send(bot_response)
            elif len(bot_response) > 2000:
                bot_response2 = bot_response[2000:]
                if len(bot_response2) <= 2000:
                    await message.channel.send(bot_response[:2000])
                    await message.channel.send(bot_response2)
                elif bot_response2 > 2000:
                    bot_response3 = bot_response2[2000:]
                    if len(bot_response3) <= 2000:
                        await message.channel.send(bot_response[:2000])
                        await message.channel.send(bot_response2[:2000])
                        await message.channel.send(bot_response3)
                    elif len(bot_response3) > 2000:
                        bot_response4 = bot_response3[2000:]
                        if len(bot_response4) <= 2000:
                            await message.channel.send(bot_response[:2000])
                            await message.channel.send(bot_response2[:2000])
                            await message.channel.send(bot_response3[:2000])
                            await message.channel.send(bot_response4)
                        elif len(bot_response4) > 2000:
                            last_response = bot_response4[2000:]
                            await message.channel.send(bot_response[:2000])
                            await message.channel.send(bot_response2[:2000])
                            await message.channel.send(bot_response3[:2000])
                            await message.channel.send(bot_response4[:2000])
                            await message.channel.send(last_response)
        elif command == "/draw":
            print(dal_prompt)
            dal_response = dalle_response(user=message_user, prompt=dal_prompt)
            await message.channel.send(dal_response[0]["url"])
            await message.channel.send(dal_response[1]["url"])
            await message.channel.send(dal_response[2]["url"])
        elif command == "/vari":
            var_response = dalle_vari(user=message_user, image=data)
            print(var_response)
            await message.channel.send(var_response[0]["url"])
            await message.channel.send(var_response[1]["url"])
    except NameError:
        return
